﻿using System.Collections.Generic;

namespace TwitterFeedApp.Domain.Users
{
    public class UserDto
    {
        public UserDto()
        {
            Following = new List<string>();
        }

        public string User { get; set; }
        public IList<string> Following { get; set; }
    }
}
