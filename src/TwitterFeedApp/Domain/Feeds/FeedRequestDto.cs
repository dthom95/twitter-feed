﻿namespace TwitterFeedApp.Domain.Feeds
{
    public class FeedRequestDto
    {
        public string UserFileName { get; set; }
        public string TweetFileName { get; set; }
    }
}
