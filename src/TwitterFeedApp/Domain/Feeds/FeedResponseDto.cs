﻿using System.Collections.Generic;
using TwitterFeedApp.Domain.Tweets;
using TwitterFeedApp.Domain.Users;

namespace TwitterFeedApp.Domain.Feeds
{
    public class FeedResponseDto
    {
        public IList<UserDto> Users { get; set; }
        public IList<TweetDto> Tweets { get; set; }
    }
}
