﻿namespace TwitterFeedApp.Domain.Tweets
{
    public class TweetDto
    {        
        public string User { get; set; }
        public string Message { get; set; }
    }
}
