﻿using System;
using log4net;
using TwitterFeedApp.Domain.Feeds;
using TwitterFeedApp.Processors;

namespace TwitterFeedApp.Services
{
    public interface ITwitterFeedService
    {
        /// <summary>
        /// Gets a feed of users and tweets.
        /// </summary>
        /// <returns>Returns a feed response</returns>
        FeedResponseDto GetTwitterFeed(FeedRequestDto request);
    }

    public class TwitterFeedService : ITwitterFeedService
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(TwitterFeedService));
        private readonly IUserFileProcessor _userFileProcessor;
        private readonly ITweetFileProcessor _tweetFileProcessor;

        public TwitterFeedService()
        {
            _userFileProcessor = new UserFileProcessor();
            _tweetFileProcessor = new TweetFileProcessor();
        }
        
        public FeedResponseDto GetTwitterFeed(FeedRequestDto request)
        {                            
            try
            {            
                _log.Debug("Calling GetTwitterFeed method");
                var response = new FeedResponseDto
                {
                    Users = _userFileProcessor.Process(request.UserFileName),
                    Tweets = _tweetFileProcessor.Process(request.TweetFileName)
                };
                return response;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                throw;
            }
        }
    }
}
