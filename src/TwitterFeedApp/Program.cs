﻿using System;
using log4net;
using log4net.Config;
using TwitterFeedApp.Domain.Feeds;
using TwitterFeedApp.Services;

namespace TwitterFeedApp
{
    class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Usage: TwitterFeedApp user.txt tweet.txt");
                return;
            }

            // register exception handler
            AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionHandler;

            // configure log4net
            XmlConfigurator.Configure();

            // log application startup
            Log.Debug("Starting application");

            var request = new FeedRequestDto
            {
                UserFileName = args[0],
                TweetFileName = args[1]
            };
            var twitterFeedService = new TwitterFeedService();
            var response = twitterFeedService.GetTwitterFeed(request);

            // display twitter feed
            foreach (var userDto in response.Users)
            {
                // display the user
                Console.WriteLine(userDto.User);

                foreach (var tweet in response.Tweets)
                {
                    // display any messages from each user or anyone they are following
                    if (userDto.User == tweet.User || userDto.Following.Contains(tweet.User))
                    {
                        Console.WriteLine("\t@{0}: {1}", tweet.User, tweet.Message);
                    }
                }
            }                    
        }

        #region Private Methods

        private static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e) {
            // handle exception
            Console.WriteLine(e.ExceptionObject);
            Environment.Exit(1);
        }

        #endregion
    }
}
