﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using FluentValidation;
using log4net;
using TwitterFeedApp.Domain.Users;
using TwitterFeedApp.Validators;

namespace TwitterFeedApp.Processors
{
    public interface IUserFileProcessor
    {
        /// <summary>
        /// Processes a user file.
        /// </summary>
        /// <param name="fileName">The name of the file</param>
        /// <returns>Returns an alphabetically ordered list of users.</returns>
        IList<UserDto> Process(string fileName);
    }

    public class UserFileProcessor : IUserFileProcessor
    {     
        private readonly ILog _log = LogManager.GetLogger(typeof(UserFileProcessor));

        public IList<UserDto> Process(string fileName)
        {                
            var users = new List<UserDto>();
            try
            {
                _log.DebugFormat("Processing file: {0}", fileName);
                // reads the whole file into an array of lines
                var lines = File.ReadAllLines(fileName, Encoding.ASCII);

                // iterates through file and creates a list of users
                lines.ToList().ForEach(line => SetUsers(line, users));

                // flattens the list into a distinct list of following users
                var following = users.SelectMany(a => a.Following)
                    .Distinct()
                    .ToList();

                // adds a new user for all users not already in the list
                foreach (var user in following)
                {
                    // skip users that already exist in the list
                    if (users.Any(a => a.User == user)) continue;
                    var userDto = new UserDto
                    {
                        User = user
                    };
                    users.Add(userDto);
                }

                // re-orders the list alphabetically by user
                users = users.OrderBy(a => a.User).ToList();

                // validate all users                
                var validator = new UserDtoValidator();
                users.ForEach(userDto => validator.ValidateAndThrow(userDto));                

                return users;
            }
            catch (Exception ex)
            {
                _log.Error("Error processing file: " + fileName, ex);
                throw;
            }            
        }

        #region Private Methods

        private static void SetUsers(string line, IList<UserDto> users)
        {
            // splits the line into users and users they follow
            var userList = line.Split(new[] {"follows"}, StringSplitOptions.RemoveEmptyEntries)
                .Select(a => a.Trim())
                .Distinct()
                .ToArray();

            if (userList.Length < 2)
            {
                throw new FormatException("User file entry is not in the correct format.");
            }

            // gets the user
            var user = userList[0];

            // creates a list of following users
            var following = userList[1].Split(',')
                .Select(a => a.Trim())
                .ToList();

            // creates a union of users and users they follow
            if (users.Any(a => a.User == user))
            {
                var userDto = users.Single(a => a.User == user);
                userDto.Following = userDto.Following.Union(following).ToList();
            }
            else
            {
                users.Add(new UserDto {User = user, Following = following});
            }
        }

        #endregion
    }
}
