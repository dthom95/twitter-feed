﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using FluentValidation;
using log4net;
using TwitterFeedApp.Domain.Tweets;
using TwitterFeedApp.Validators;

namespace TwitterFeedApp.Processors
{
    public interface ITweetFileProcessor
    {    
        /// <summary>
        /// Processes the tweet file.
        /// </summary>
        /// <param name="fileName">The name of the file</param>
        /// <returns>Returns a list of tweets.</returns>
        IList<TweetDto> Process(string fileName);
    }

    public class TweetFileProcessor : ITweetFileProcessor
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(TweetFileProcessor));

        public IList<TweetDto> Process(string fileName)
        {                            
            var tweets = new List<TweetDto>();                
            try
            {            
                _log.DebugFormat("Processing file: {0}", fileName);
                // reads the whole file into an array of lines                
                var lines = File.ReadAllLines(fileName, Encoding.ASCII);                

                // iterates through file and creates a list of tweets
                lines.ToList().ForEach(line => SetTweets(line, tweets));                                                

                // validate all tweets                
                var validator = new TweetDtoValidator();                
                tweets.ForEach(tweetDto => validator.ValidateAndThrow(tweetDto));                

                return tweets;
            }
            catch (Exception ex)
            {
                _log.Error("Error processing file: " + fileName, ex);
                throw;
            }                        
        }

        #region Private Methods

        private static void SetTweets(string line, IList<TweetDto> tweets)
        {
            // splits the tweets into users and messages
            var userList = line.Split(new[] { ">" }, StringSplitOptions.RemoveEmptyEntries)
                .Select(a => a.Trim())
                .ToArray();

            if (userList.Length < 2)
            {
                throw new FormatException("Tweet file entry is not in the correct format.");
            }

            var tweetDto = new TweetDto
            {
                User = userList[0],
                Message = userList[1]
            };
            tweets.Add(tweetDto);
        }

        #endregion                
    }
}
