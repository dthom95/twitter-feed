﻿using FluentValidation;
using TwitterFeedApp.Domain.Users;

namespace TwitterFeedApp.Validators
{
    public class UserDtoValidator : AbstractValidator<UserDto>
    {
        public UserDtoValidator()
        {
            RuleFor(a => a.User).NotEmpty();
        }
    }
}
