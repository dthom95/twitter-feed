﻿using FluentValidation;
using TwitterFeedApp.Domain.Tweets;

namespace TwitterFeedApp.Validators
{
    public class TweetDtoValidator : AbstractValidator<TweetDto>
    {
        public TweetDtoValidator()
        {
            RuleFor(a => a.User).NotEmpty();
            RuleFor(a => a.Message).NotEmpty().MaximumLength(140);
        }
    }
}
