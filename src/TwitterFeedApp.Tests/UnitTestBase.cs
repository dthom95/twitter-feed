﻿using System.IO;
using log4net;
using log4net.Config;
using NUnit.Framework;

namespace TwitterFeedApp.Tests
{    
    public class UnitTestBase
    {
        protected static ILog Log = LogManager.GetLogger(typeof(UnitTestBase));
        protected string DataDirectory
        {
            get
            {
                return Path.Combine(TestContext.CurrentContext.TestDirectory, "Data");
            }
        }

        static UnitTestBase()
        {    
            // Set up a simple configuration that logs on the console.
            BasicConfigurator.Configure();
        }
    }
}
