﻿using System.IO;
using System.Linq;
using NUnit.Framework;
using TwitterFeedApp.Processors;

namespace TwitterFeedApp.Tests.Processors
{
    [TestFixture]
    public class TweetFileProcessorTests : UnitTestBase
    {
        [Test]
        public void TweetFile_Process()
        {
            var tweetFileProcessor = new TweetFileProcessor();
            var fileName = Path.Combine(DataDirectory, "tweet.txt");            
            var tweets = tweetFileProcessor.Process(fileName);
            // check if there are users
            Assert.IsNotNull(tweets);
            Assert.IsNotEmpty(tweets);
            // check that the users are not empty
            Assert.IsNotEmpty(tweets.Select(a => a.User));            
            // check that the messages are not empty
            Assert.IsNotEmpty(tweets.Select(a => a.Message));            
        }                   
    }
}
