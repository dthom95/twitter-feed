﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using TwitterFeedApp.Processors;

namespace TwitterFeedApp.Tests.Processors
{
    [TestFixture]
    public class UserFileProcessorTests : UnitTestBase
    {
        [Test]
        public void UserFile_Process()
        {
            var userFileProcessor = new UserFileProcessor();
            var fileName = Path.Combine(DataDirectory, "user.txt");
            var users = userFileProcessor.Process(fileName);
            // check if there are users
            Assert.IsNotNull(users);                        
            Assert.IsNotEmpty(users);                   
            // check that the users are not empty
            Assert.IsNotEmpty(users.Select(a => a.User));            
        }         
    }
}
