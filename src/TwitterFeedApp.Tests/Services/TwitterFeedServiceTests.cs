﻿using NUnit.Framework;
using System.IO;
using System.Linq;
using TwitterFeedApp.Domain.Feeds;
using TwitterFeedApp.Services;

namespace TwitterFeedApp.Tests.Services
{
    [TestFixture]
    public class TwitterFeedServiceTests : UnitTestBase
    {
        [Test]
        public void GetTwitterFeed()
        {
            var twitterFeedService = new TwitterFeedService();
            var request = new FeedRequestDto
            {
                UserFileName = Path.Combine(DataDirectory, "user.txt"),
                TweetFileName = Path.Combine(DataDirectory, "tweet.txt")
            };            
            var response = twitterFeedService.GetTwitterFeed(request);
            // check if there is a response
            Assert.IsNotNull(response);
            // check if there are users
            Assert.IsNotEmpty(response.Users);
            // check if there are tweets
            Assert.IsNotEmpty(response.Tweets);
            // check if all tweet users exist in the user file
            Assert.IsTrue(response.Tweets.Select(a => a.User)
                .All(value => response.Users.Select(a => a.User)
                .Contains(value)));
            // check if all followers exist in the user file
            Assert.IsTrue(response.Users.SelectMany(a => a.Following)
                .All(value => response.Users.Select(a => a.User)
                .Contains(value)));
        }
    }
}
