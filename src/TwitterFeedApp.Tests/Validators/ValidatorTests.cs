﻿using NUnit.Framework;
using TwitterFeedApp.Domain.Tweets;
using TwitterFeedApp.Domain.Users;
using TwitterFeedApp.Validators;

namespace TwitterFeedApp.Tests.Validators
{
    [TestFixture]
    public class ValidatorTests : UnitTestBase
    {
        [Test]
        public void UserDto_Validator_Valid()
        {
            var userDto = new UserDto { User = "Martin" };
            var validator = new UserDtoValidator();
            var results = validator.Validate(userDto);
            Assert.IsTrue(results.IsValid);
        }

        [Test]
        public void TweetDto_Validator_Valid()
        {
            var tweetDto = new TweetDto
            {
                User = "Alan",
                Message = "If you have a procedure with 10 parameters, you probably missed some."
            };
            var validator = new TweetDtoValidator();
            var results = validator.Validate(tweetDto);            
            Assert.IsTrue(results.IsValid);
        }

        [Test]
        public void TweetDto_Validator_Invalid()
        {
            var tweetDto = new TweetDto
            {
                User = "Ward",
                Message = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            };
            var validator = new TweetDtoValidator();
            var results = validator.Validate(tweetDto);            
            Assert.IsFalse(results.IsValid);
        }
    }
}
