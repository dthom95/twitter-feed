SET PATH=C:\Windows\Microsoft.NET\Framework\v4.0.30319\
"tools/nuget.exe" restore src/TwitterFeed.sln
msbuild src/TwitterFeed.sln /t:TwitterFeedApp /p:Configuration=Release /p:OutputPath="..\..\build"
copy data\*.txt build
pause