# README #

### What is this repository for? ###

* This repository is a Twitter like Feed.

### How do I get set up? ###

* The code was built using Visual Studio 2017 using .NET 4.6.1.
* Download the code using the command git clone https://bitbucket.org/dthom95/twitter-feed.
* Build the solution which will restore the NuGet dependencies.
* Download the NUnit 3 test adapter to run the unit tests. https://marketplace.visualstudio.com/items?itemName=NUnitDevelopers.NUnit3TestAdapter.
* Navigate to Test => Windows => Test Explorer to run the tests.
* There is build folder with the latest build and a batch file for executing a build.
* Navigate into the build folder and run the command: TwitterFeedApp user.txt tweet.txt.

### Assumptions ###

* The user.txt files and tweet.txt files have valid entries.
* Any deviations from the format will throw an exception.
* All of the data is processed in memory. Because the data needs to be sorted alphabetically on the console, you have to process all of the data in the files.
* I would recommend batching the data instead, but then it would not be sorted unless you put the data in a database.
* Fluent Validation is used for validation of user and tweet data after it has been processed.
* Any issues with the data will throw an exception.
* All debugging info and exception handling uses Log4Net. Logs are configured with minimal locking, so that you can delete the files while the application is running if necessary.
* The log level is set to debug, but you can change this in the Log4Net.xml if you don't want debugging info in production.
* Dependency injection is not configured, but can easily be added if the application becomes more complex.
* All services and processors have interfaces to enable dependency injection in the future.